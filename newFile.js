class HelloMessage extends React.Component {
  render() {
    return (
      <div>
        Hello World
      </div>
    );
  }
}

ReactDOM.render(
  <HelloMessage />,
  document.getElementById('hello-example')
);

